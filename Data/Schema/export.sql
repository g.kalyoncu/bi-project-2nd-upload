--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 11.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Analytics; Type: TABLE; Schema: public; Owner: team7admin
--

CREATE TABLE public."Analytics" (
    "CSF_id" integer,
    "KPI_id" integer,
    country_id integer,
    year integer,
    value double precision
);


ALTER TABLE public."Analytics" OWNER TO team7admin;

--
-- Name: CSF; Type: TABLE; Schema: public; Owner: team7admin
--

CREATE TABLE public."CSF" (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public."CSF" OWNER TO team7admin;

--
-- Name: Countries; Type: TABLE; Schema: public; Owner: team7admin
--

CREATE TABLE public."Countries" (
    id integer NOT NULL,
    "countryName" text NOT NULL
);


ALTER TABLE public."Countries" OWNER TO team7admin;

--
-- Name: DIM_Italy; Type: TABLE; Schema: public; Owner: team7admin
--

CREATE TABLE public."DIM_Italy" (
    year integer NOT NULL,
    "PercentageFemaleStudents" double precision,
    "PercentageMaleStudents" double precision,
    "PercentageFemaleStaff" double precision,
    "PercentageMaleStaff" double precision,
    "EducationSpending" double precision,
    "GDPPerCapita" integer,
    "PercentageInternationalStudents" double precision,
    "PercentageHouseholdSpending" double precision
);


ALTER TABLE public."DIM_Italy" OWNER TO team7admin;

--
-- Name: DIM_Netherlands; Type: TABLE; Schema: public; Owner: team7admin
--

CREATE TABLE public."DIM_Netherlands" (
    year integer NOT NULL,
    "PercentageFemaleStudents" double precision,
    "PercentageMaleStudents" double precision,
    "PercentageFemaleStaff" double precision,
    "PercentageMaleStaff" double precision,
    "EducationSpending" double precision,
    "GDPPerCapita" integer,
    "PercentageInternationalStudents" double precision,
    "PercentageHouseholdSpending" double precision
);


ALTER TABLE public."DIM_Netherlands" OWNER TO team7admin;

--
-- Name: DIM_Turkey; Type: TABLE; Schema: public; Owner: team7admin
--

CREATE TABLE public."DIM_Turkey" (
    year integer NOT NULL,
    "PercentageFemaleStudents" double precision,
    "PercentageMaleStudents" double precision,
    "PercentageFemaleStaff" double precision,
    "PercentageMaleStaff" double precision,
    "EducationSpending" double precision,
    "GDPPerCapita" integer,
    "PercentageInternationalStudents" double precision,
    "PercentageHouseholdSpending" double precision
);


ALTER TABLE public."DIM_Turkey" OWNER TO team7admin;

--
-- Name: DIM_USA; Type: TABLE; Schema: public; Owner: team7admin
--

CREATE TABLE public."DIM_USA" (
    year integer NOT NULL,
    "PercentageFemaleStudents" double precision,
    "PercentageMaleStudents" double precision,
    "PercentageFemaleStaff" double precision,
    "PercentageMaleStaff" double precision,
    "EducationSpending" double precision,
    "GDPPerCapita" integer,
    "PercentageInternationalStudents" double precision,
    "PercentageHouseholdSpending" double precision
);


ALTER TABLE public."DIM_USA" OWNER TO team7admin;

--
-- Name: Italy; Type: TABLE; Schema: public; Owner: team7admin
--

CREATE TABLE public."Italy" (
    "numOfStudents" integer,
    "numOfMaleStudents" integer,
    "numOfFemaleStudents" integer,
    "numOfEmployees" integer,
    "numOfMaleEmployees" integer,
    "numOfFemaleEmployees" integer,
    "numOfGraduates" integer,
    "numOfMaleGraduates" integer,
    "numOfFemaleGraduates" integer,
    "numOfUniversities" integer,
    "numOfTop100" integer,
    population integer,
    "graduationRate" integer,
    "graduationTime" integer,
    "graduationDelay" integer,
    "studentCost" integer,
    "governmentFund" bigint,
    "GDP" integer,
    year integer NOT NULL
);


ALTER TABLE public."Italy" OWNER TO team7admin;

--
-- Name: KPI; Type: TABLE; Schema: public; Owner: team7admin
--

CREATE TABLE public."KPI" (
    id integer NOT NULL,
    "CSF_id" integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public."KPI" OWNER TO team7admin;

--
-- Name: Netherlands; Type: TABLE; Schema: public; Owner: team7admin
--

CREATE TABLE public."Netherlands" (
    "numOfStudents" integer,
    "numOfMaleStudents" integer,
    "numOfFemaleStudents" integer,
    "numOfEmployees" integer,
    "numOfMaleEmployees" integer,
    "numOfFemaleEmployees" integer,
    "numOfGraduates" integer,
    "numOfMaleGraduates" integer,
    "numOfFemaleGraduates" integer,
    "numOfUniversities" integer,
    "numOfTop100" integer,
    population integer,
    "graduationRate" integer,
    "graduationTime" integer,
    "graduationDelay" integer,
    "studentCost" integer,
    "governmentFund" bigint,
    "GDP" integer,
    year integer NOT NULL
);


ALTER TABLE public."Netherlands" OWNER TO team7admin;

--
-- Name: Test; Type: TABLE; Schema: public; Owner: team7admin
--

CREATE TABLE public."Test" (
    country integer,
    "numOfStudents" integer,
    "numOfTop100Uni" integer,
    year integer,
    "studentCost" integer
);


ALTER TABLE public."Test" OWNER TO team7admin;

--
-- Name: Turkey; Type: TABLE; Schema: public; Owner: team7admin
--

CREATE TABLE public."Turkey" (
    "numOfStudents" integer,
    "numOfMaleStudents" integer,
    "numOfFemaleStudents" integer,
    "numOfEmployees" integer,
    "numOfMaleEmployees" integer,
    "numOfFemaleEmployees" integer,
    "numOfGraduates" integer,
    "numOfMaleGraduates" integer,
    "numOfFemaleGraduates" integer,
    "numOfUniversities" integer,
    "numOfTop100" integer,
    population integer,
    "graduationRate" integer,
    "graduationTime" integer,
    "graduationDelay" integer,
    "studentCost" integer,
    "governmentFund" bigint,
    "GDP" integer,
    year integer NOT NULL
);


ALTER TABLE public."Turkey" OWNER TO team7admin;

--
-- Name: USA; Type: TABLE; Schema: public; Owner: team7admin
--

CREATE TABLE public."USA" (
    "numOfStudents" integer,
    "numOfMaleStudents" integer,
    "numOfFemaleStudents" integer,
    "numOfEmployees" integer,
    "numOfMaleEmployees" integer,
    "numOfFemaleEmployees" integer,
    "numOfGraduates" integer,
    "numOfMaleGraduates" integer,
    "numOfFemaleGraduates" integer,
    "numOfUniversities" integer,
    "numOfTop100" integer,
    population integer,
    "graduationRate" integer,
    "graduationTime" integer,
    "graduationDelay" integer,
    "studentCost" integer,
    "governmentFund" bigint,
    "GDP" integer,
    year integer NOT NULL
);


ALTER TABLE public."USA" OWNER TO team7admin;

--
-- Name: Years; Type: TABLE; Schema: public; Owner: team7admin
--

CREATE TABLE public."Years" (
    year integer NOT NULL
);


ALTER TABLE public."Years" OWNER TO team7admin;

--
-- Data for Name: Analytics; Type: TABLE DATA; Schema: public; Owner: team7admin
--

COPY public."Analytics" ("CSF_id", "KPI_id", country_id, year, value) FROM stdin;
1	2	1	2009	\N
1	2	1	2010	39.5799999999999983
1	1	2	2009	57.1700000000000017
1	1	2	2010	57.2199999999999989
1	1	2	2011	57.1099999999999994
1	1	2	2012	57.0600000000000023
1	1	2	2013	56.8699999999999974
1	1	2	2014	56.5399999999999991
1	1	2	2015	56.25
1	1	2	2016	55.9299999999999997
1	1	2	2017	55.5600000000000023
1	1	2	2018	\N
1	2	2	2009	\N
1	2	2	2010	35.5799999999999983
1	2	2	2011	36.2199999999999989
1	2	2	2012	36.4699999999999989
1	2	2	2013	36.8900000000000006
1	2	2	2014	37.4699999999999989
1	2	2	2015	37.2800000000000011
1	2	2	2016	37.0300000000000011
1	2	2	2017	\N
1	2	2	2018	\N
1	2	1	2011	39.509999999999998
1	2	1	2012	40
1	2	1	2013	43.1799999999999997
1	2	1	2014	43.509999999999998
1	2	1	2015	44.3599999999999994
1	2	1	2016	45.2199999999999989
1	2	1	2017	\N
1	2	1	2018	\N
1	1	1	2009	51.9299999999999997
1	1	1	2010	52.0300000000000011
1	1	1	2011	51.759999999999998
1	1	1	2012	51.6300000000000026
1	1	1	2013	51.4399999999999977
1	1	1	2014	51.3599999999999994
1	1	1	2015	51.240000000000002
1	1	1	2016	51.259999999999998
1	1	1	2017	51.240000000000002
1	1	1	2018	\N
1	1	3	2009	56.6899999999999977
1	1	3	2010	56.6599999999999966
1	1	3	2011	56.7199999999999989
1	1	3	2012	56.5
1	1	3	2013	56.1700000000000017
1	1	3	2014	56.1300000000000026
1	1	3	2015	55.990000000000002
1	1	3	2016	56.0499999999999972
1	1	3	2017	56.1599999999999966
1	1	3	2018	56.1499999999999986
1	2	3	2009	\N
1	2	3	2010	46.5200000000000031
1	2	3	2011	48.0700000000000003
1	2	3	2012	48.5900000000000034
1	2	3	2013	48.5900000000000034
1	2	3	2014	49.1099999999999994
1	2	3	2015	49.1099999999999994
1	2	3	2016	49.4399999999999977
1	2	3	2017	\N
1	2	3	2018	\N
1	2	4	2009	\N
1	2	4	2010	40.9099999999999966
1	2	4	2011	40.8999999999999986
1	2	4	2012	41.1300000000000026
1	2	4	2013	41.5600000000000023
1	2	4	2014	42.7800000000000011
1	2	4	2015	43.0600000000000023
1	2	4	2016	43.2700000000000031
1	2	4	2017	\N
1	2	4	2018	\N
1	1	4	2009	45.4699999999999989
1	1	4	2010	46.2899999999999991
1	1	4	2011	46.6400000000000006
1	1	4	2012	46.8400000000000034
1	1	4	2013	47.1499999999999986
1	1	4	2014	47.4200000000000017
1	1	4	2015	47.509999999999998
1	1	4	2016	47.9200000000000017
1	1	4	2017	48.2700000000000031
1	1	4	2018	\N
2	4	3	2009	\N
2	4	3	2010	\N
2	4	3	2011	\N
2	4	3	2012	\N
2	4	3	2013	\N
2	4	3	2014	\N
2	4	3	2015	\N
2	4	3	2016	\N
2	4	3	2017	\N
2	4	3	2018	\N
2	4	4	2009	\N
2	4	4	2010	\N
2	4	4	2011	\N
2	4	4	2012	\N
2	4	4	2013	\N
2	4	4	2014	\N
2	4	4	2015	\N
2	4	4	2016	\N
2	4	4	2017	\N
2	4	4	2018	\N
2	4	2	2009	\N
2	4	2	2010	\N
2	4	2	2011	\N
2	4	2	2012	\N
2	4	2	2013	\N
2	4	2	2014	\N
2	4	2	2015	\N
2	4	2	2016	\N
2	3	3	2009	\N
2	3	3	2010	\N
2	3	3	2011	\N
2	3	3	2012	\N
2	3	3	2013	3.8660000000000001
2	3	3	2014	4.20999999999999996
2	3	3	2015	4.64499999999999957
2	3	3	2016	5.03599999999999959
2	3	3	2017	\N
2	3	4	2009	\N
2	3	4	2010	\N
2	3	4	2011	\N
2	3	4	2012	\N
2	3	4	2013	1.09299999999999997
2	3	4	2014	0.880000000000000004
2	3	4	2015	1.18999999999999995
2	3	4	2016	1.31400000000000006
2	3	4	2017	\N
3	6	4	2009	\N
3	6	4	2010	\N
3	6	4	2011	\N
3	6	4	2012	14.1099999999999994
3	6	4	2013	10.6699999999999999
3	6	4	2014	13.0299999999999994
3	6	4	2015	12.4499999999999993
3	6	4	2016	\N
3	6	3	2009	\N
3	6	3	2010	\N
3	6	3	2011	\N
3	6	3	2012	45.8500000000000014
3	6	3	2013	46.5
3	6	3	2014	46.1899999999999977
3	6	3	2015	46.2999999999999972
3	6	3	2016	\N
3	6	3	2017	\N
3	6	2	2009	\N
3	6	2	2010	\N
3	6	2	2011	\N
3	6	2	2012	25.7800000000000011
3	6	2	2013	25.4200000000000017
2	4	2	2017	\N
2	4	2	2018	\N
2	4	1	2009	\N
2	4	1	2010	\N
2	4	1	2011	\N
2	4	1	2012	\N
2	4	1	2013	\N
2	4	1	2014	\N
2	4	1	2015	\N
2	4	1	2016	\N
2	4	1	2017	\N
2	4	1	2018	\N
2	3	1	2009	\N
2	3	1	2010	\N
2	3	1	2011	\N
2	3	1	2012	\N
2	3	1	2013	10.218
2	3	1	2014	10.0670000000000002
2	3	1	2015	10.2289999999999992
3	5	1	2009	39.5038930150111085
3	5	1	2010	39.1125863102506628
3	5	1	2011	39.0023391059894067
3	5	1	2012	39.6805720088001337
3	5	1	2013	39.7792579655991716
3	5	2	2009	28.5540690541566882
3	5	2	2010	29.01542453510163
3	5	2	2011	29.709475441769861
3	5	2	2012	29.8386329949380524
3	5	2	2013	31.4983976591890773
3	5	2	2014	31.7121787585595065
3	5	2	2015	30.5600499511347614
3	5	2	2016	\N
3	5	3	2009	57.4043123245725937
3	5	3	2010	53.0667024837789825
3	5	3	2011	52.6162650602409627
3	5	3	2012	53.4286989771161274
3	5	3	2013	52.0199562396257775
3	5	3	2014	53.3870938381723832
3	5	3	2015	52.9147634080527638
3	5	3	2016	\N
3	5	4	2009	\N
3	5	4	2010	\N
3	5	4	2011	60.7822062226793562
3	5	4	2012	49.4119083671176682
3	5	4	2013	46.3782931772123348
3	5	4	2014	43.3340282700245965
3	5	4	2015	34.5953824626865725
3	5	4	2016	\N
3	5	1	2014	39.0676984949119515
3	5	1	2015	38.3408214385113908
3	5	1	2016	\N
3	5	1	2017	\N
3	5	1	2018	\N
3	5	2	2017	\N
3	5	2	2018	\N
3	5	3	2017	\N
3	5	3	2018	\N
3	5	4	2017	\N
3	5	4	2018	\N
3	6	4	2017	\N
3	6	4	2018	\N
3	6	3	2018	\N
3	6	2	2014	26.5
3	6	2	2015	28.2399999999999984
3	6	2	2016	\N
3	6	2	2017	\N
3	6	2	2018	\N
3	6	1	2009	\N
3	6	1	2010	\N
3	6	1	2011	\N
3	6	1	2012	15.1600000000000001
3	6	1	2013	15.4700000000000006
3	6	1	2014	15.8399999999999999
3	6	1	2015	16.129999999999999
3	6	1	2016	\N
3	6	1	2017	\N
3	6	1	2018	\N
2	3	1	2016	10.7439999999999998
2	3	1	2017	\N
2	3	1	2018	\N
2	3	2	2009	\N
2	3	2	2010	\N
2	3	2	2011	\N
2	3	2	2012	\N
2	3	2	2013	4.25499999999999989
2	3	2	2014	4.56200000000000028
2	3	2	2015	4.95000000000000018
2	3	2	2016	5.10200000000000031
2	3	2	2017	\N
2	3	2	2018	\N
2	3	3	2018	\N
2	3	4	2018	\N
\.


--
-- Data for Name: CSF; Type: TABLE DATA; Schema: public; Owner: team7admin
--

COPY public."CSF" (id, name) FROM stdin;
1	Low Gender Gap in the universities
2	High International Openness
3	Low Higher Education Cost
\.


--
-- Data for Name: Countries; Type: TABLE DATA; Schema: public; Owner: team7admin
--

COPY public."Countries" (id, "countryName") FROM stdin;
1	Netherlands
2	Italy
3	USA
4	Turkey
\.


--
-- Data for Name: DIM_Italy; Type: TABLE DATA; Schema: public; Owner: team7admin
--

COPY public."DIM_Italy" (year, "PercentageFemaleStudents", "PercentageMaleStudents", "PercentageFemaleStaff", "PercentageMaleStaff", "EducationSpending", "GDPPerCapita", "PercentageInternationalStudents", "PercentageHouseholdSpending") FROM stdin;
2009	57.1700000000000017	42.8299999999999983	\N	\N	9775.20000000000073	34234	\N	\N
2010	57.2199999999999989	42.7800000000000011	35.5799999999999983	64.4200000000000017	10064	34685	\N	\N
2011	57.1099999999999994	42.8900000000000006	36.2199999999999989	63.7800000000000011	10676.1000000000004	35935	\N	\N
2012	57.0600000000000023	42.9399999999999977	36.4699999999999989	63.5300000000000011	10669.3999999999996	35757	\N	25.7800000000000011
2013	56.8699999999999974	43.1300000000000026	36.8900000000000006	63.1099999999999994	11303.2000000000007	35885	4.25499999999999989	25.4200000000000017
2014	56.5399999999999991	43.4600000000000009	37.4699999999999989	62.5300000000000011	11438.8999999999996	36071	4.56200000000000028	26.5
2015	56.25	43.75	37.2800000000000011	62.7199999999999989	11257.1000000000004	36836	4.95000000000000018	28.2399999999999984
2016	55.9299999999999997	44.0700000000000003	37.0300000000000011	62.9699999999999989	\N	39045	5.10200000000000031	\N
2017	55.5600000000000023	44.4399999999999977	\N	\N	\N	40981	\N	\N
2018	\N	\N	\N	\N	\N	41626	\N	\N
\.


--
-- Data for Name: DIM_Netherlands; Type: TABLE DATA; Schema: public; Owner: team7admin
--

COPY public."DIM_Netherlands" (year, "PercentageFemaleStudents", "PercentageMaleStudents", "PercentageFemaleStaff", "PercentageMaleStaff", "EducationSpending", "GDPPerCapita", "PercentageInternationalStudents", "PercentageHouseholdSpending") FROM stdin;
2009	51.9299999999999997	48.0700000000000003	\N	\N	17605.7000000000007	44567	\N	\N
2010	52.0300000000000011	47.9699999999999989	39.5799999999999983	60.4200000000000017	17616.7000000000007	45041	\N	\N
2011	51.759999999999998	48.240000000000002	39.509999999999998	60.490000000000002	18174.7000000000007	46599	\N	\N
2012	51.6300000000000026	48.3699999999999974	40	60	18757.7999999999993	47272	\N	15.1600000000000001
2013	51.4399999999999977	48.5600000000000023	43.1799999999999997	56.8200000000000003	19588.5	49243	10.218	15.4700000000000006
2014	51.3599999999999994	48.6400000000000006	43.509999999999998	56.490000000000002	19234.2000000000007	49233	10.0670000000000002	15.8399999999999999
2015	51.240000000000002	48.759999999999998	44.3599999999999994	55.6400000000000006	19286.2000000000007	50302	10.2289999999999992	16.129999999999999
2016	51.259999999999998	48.740000000000002	45.2199999999999989	54.7800000000000011	\N	51340	10.7439999999999998	\N
2017	51.240000000000002	48.759999999999998	\N	\N	\N	54423	\N	\N
2018	\N	\N	\N	\N	\N	56277	\N	\N
\.


--
-- Data for Name: DIM_Turkey; Type: TABLE DATA; Schema: public; Owner: team7admin
--

COPY public."DIM_Turkey" (year, "PercentageFemaleStudents", "PercentageMaleStudents", "PercentageFemaleStaff", "PercentageMaleStaff", "EducationSpending", "GDPPerCapita", "PercentageInternationalStudents", "PercentageHouseholdSpending") FROM stdin;
2009	45.4699999999999989	54.5300000000000011	\N	\N	\N	15336	\N	\N
2010	46.2899999999999991	53.7100000000000009	40.9099999999999966	59.0900000000000034	\N	17232	\N	\N
2011	46.6400000000000006	53.3599999999999994	40.8999999999999986	59.1000000000000014	11819.1000000000004	19445	\N	\N
2012	46.8400000000000034	53.1599999999999966	41.1300000000000026	58.8699999999999974	10116.1000000000004	20473	\N	14.1099999999999994
2013	47.1499999999999986	52.8500000000000014	41.5600000000000023	58.4399999999999977	10298.2999999999993	22205	1.09299999999999997	10.6699999999999999
2014	47.4200000000000017	52.5799999999999983	42.7800000000000011	57.2199999999999989	10392.7999999999993	23983	0.880000000000000004	13.0299999999999994
2015	47.509999999999998	52.490000000000002	43.0600000000000023	56.9399999999999977	8900.70000000000073	25728	1.18999999999999995	12.4499999999999993
2016	47.9200000000000017	52.0799999999999983	43.2700000000000031	56.7299999999999969	\N	26330	1.31400000000000006	\N
2017	48.2700000000000031	51.7299999999999969	\N	\N	\N	28152	\N	\N
2018	\N	\N	\N	\N	\N	28205	\N	\N
\.


--
-- Data for Name: DIM_USA; Type: TABLE DATA; Schema: public; Owner: team7admin
--

COPY public."DIM_USA" (year, "PercentageFemaleStudents", "PercentageMaleStudents", "PercentageFemaleStaff", "PercentageMaleStaff", "EducationSpending", "GDPPerCapita", "PercentageInternationalStudents", "PercentageHouseholdSpending") FROM stdin;
2009	56.6899999999999977	43.3100000000000023	\N	\N	26996.0999999999985	47028	\N	\N
2010	56.6599999999999966	43.3400000000000034	46.5200000000000031	53.4799999999999969	25681.0999999999985	48394	\N	\N
2011	56.7199999999999989	43.2800000000000011	48.0700000000000003	51.9299999999999997	26202.9000000000015	49800	\N	\N
2012	56.5	43.5	48.5900000000000034	51.4099999999999966	27527	51521	\N	45.8500000000000014
2013	56.1700000000000017	43.8299999999999983	48.5900000000000034	51.4099999999999966	27578.9000000000015	53016	3.8660000000000001	46.5
2014	56.1300000000000026	43.8699999999999974	49.1099999999999994	50.8900000000000006	29328.2000000000007	54935	4.20999999999999996	46.1899999999999977
2015	55.990000000000002	44.009999999999998	49.1099999999999994	50.8900000000000006	30003.2000000000007	56701	4.64499999999999957	46.2999999999999972
2016	56.0499999999999972	43.9500000000000028	49.4399999999999977	50.5600000000000023	\N	57797	5.03599999999999959	\N
2017	56.1599999999999966	43.8400000000000034	\N	\N	\N	59774	\N	\N
2018	56.1499999999999986	43.8500000000000014	\N	\N	\N	62480	\N	\N
\.


--
-- Data for Name: Italy; Type: TABLE DATA; Schema: public; Owner: team7admin
--

COPY public."Italy" ("numOfStudents", "numOfMaleStudents", "numOfFemaleStudents", "numOfEmployees", "numOfMaleEmployees", "numOfFemaleEmployees", "numOfGraduates", "numOfMaleGraduates", "numOfFemaleGraduates", "numOfUniversities", "numOfTop100", population, "graduationRate", "graduationTime", "graduationDelay", "studentCost", "governmentFund", "GDP", year) FROM stdin;
\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	59000000	\N	\N	\N	\N	\N	34513	2009
\N	\N	\N	257332	11878	245454	217738	90102	127636	30	0	60480000	\N	\N	\N	\N	66425400000	39636	2018
9308902	3347569	5961333	252806	11063	241743	214758	89066	125692	30	0	60590000	61	\N	\N	\N	67263300000	40923	2017
8806502	3994871	4811631	254572	10501	244071	204260	84584	119676	28	0	60670000	61	\N	\N	\N	68922500000	39043	2016
9701002	3702867	5998135	237483	9803	227680	203771	83340	120431	26	0	60800000	60	\N	\N	\N	71219800000	36836	2015
9849060	4886229	4962831	237214	9768	227446	207709	83169	120761	26	0	60780000	60	\N	\N	\N	65891100000	36070	2014
5539531	2575803	2963728	237735	9650	228085	201652	80891	120761	24	0	59690000	59	\N	\N	\N	68156800000	36131	2013
9886380	4925049	4961331	237502	9210	228292	199825	78989	120836	24	0	59390000	61	\N	\N	\N	61152200000	36237	2012
8982349	3764131	5218218	\N	\N	\N	192293	80245	112048	\N	0	59360000	60	\N	\N	\N	61460900000	36347	2011
8218236	4156609	4061627	\N	\N	\N	188239	79259	108980	\N	0	59190000	58	\N	\N	\N	62669100000	35008	2010
\.


--
-- Data for Name: KPI; Type: TABLE DATA; Schema: public; Owner: team7admin
--

COPY public."KPI" (id, "CSF_id", name) FROM stdin;
1	1	Low Gender Gap among Students
2	1	Low Gender Gap among Employees
3	2	Number of International Students
4	2	Number of International Employees
5	3	The relation of GDP to the education spending
6	3	Education spending covered by students and their families
\.


--
-- Data for Name: Netherlands; Type: TABLE DATA; Schema: public; Owner: team7admin
--

COPY public."Netherlands" ("numOfStudents", "numOfMaleStudents", "numOfFemaleStudents", "numOfEmployees", "numOfMaleEmployees", "numOfFemaleEmployees", "numOfGraduates", "numOfMaleGraduates", "numOfFemaleGraduates", "numOfUniversities", "numOfTop100", population, "graduationRate", "graduationTime", "graduationDelay", "studentCost", "governmentFund", "GDP", year) FROM stdin;
292700	141300	151400	42800	28900	13900	\N	\N	\N	20	2	17132908	\N	\N	\N	2060	\N	56383	2018
278000	135400	142600	42300	28700	13600	\N	\N	\N	20	2	17035938	\N	\N	\N	2006	5210000	53597	2017
266000	130200	135800	42200	28600	13600	\N	\N	\N	19	4	16987330	\N	\N	\N	1984	508200	52267	2016
259400	127300	132100	41050	28400	12650	\N	\N	\N	19	6	16938499	\N	\N	\N	1951	4978000	51410	2015
254700	124300	130400	40500	27800	12700	\N	\N	\N	19	6	16889356	\N	\N	\N	1906	4858000	50497	2014
249500	120800	128700	40100	27900	12200	\N	\N	\N	12	6	16839699	\N	\N	\N	1835	4825000	49969	2013
248300	120100	128200	39700	28400	11300	\N	\N	\N	12	4	16789095	\N	\N	\N	1771	4749000	50212	2012
239800	116100	123700	39700	28300	11400	\N	\N	\N	12	3	16737002	\N	\N	\N	1713	4641000	50937	2011
243800	117800	126000	39800	28300	11500	\N	\N	\N	12	4	16682917	\N	\N	\N	1672	4518000	50338	2010
240800	116300	124500	39101	28500	10600	\N	\N	\N	12	4	16626926	\N	\N	\N	1620	4352000	49897	2009
\.


--
-- Data for Name: Test; Type: TABLE DATA; Schema: public; Owner: team7admin
--

COPY public."Test" (country, "numOfStudents", "numOfTop100Uni", year, "studentCost") FROM stdin;
1	130000	10	2009	500
1	145000	9	2010	500
1	155000	11	2011	650
1	150000	10	2012	650
1	210000	10	2013	650
1	180000	13	2014	700
1	190000	11	2015	700
1	200000	9	2016	750
1	210000	10	2017	750
1	200000	9	2018	750
2	250000	7	2009	1500
2	265000	9	2010	1500
2	260000	5	2011	1650
2	310000	5	2012	1650
2	320000	5	2013	1650
2	330000	3	2014	1700
2	340000	6	2015	1700
2	330000	9	2016	1750
2	310000	7	2017	1750
2	380000	9	2018	1750
3	410000	1	2009	15000
3	415000	3	2010	15000
3	430000	3	2011	16500
3	500000	3	2012	16500
3	600000	3	2013	16500
3	550000	3	2014	17000
3	530000	2	2015	17000
3	480000	2	2016	17500
3	460000	3	2017	17500
3	430000	1	2018	17500
4	650000	4	2009	8000
4	660000	3	2010	8000
4	670000	2	2011	9000
4	690000	2	2012	8500
4	700000	2	2013	8500
4	750000	3	2014	8500
4	720000	1	2015	9500
4	730000	2	2016	9500
4	720000	2	2017	9500
4	730000	2	2018	10000
\.


--
-- Data for Name: Turkey; Type: TABLE DATA; Schema: public; Owner: team7admin
--

COPY public."Turkey" ("numOfStudents", "numOfMaleStudents", "numOfFemaleStudents", "numOfEmployees", "numOfMaleEmployees", "numOfFemaleEmployees", "numOfGraduates", "numOfMaleGraduates", "numOfFemaleGraduates", "numOfUniversities", "numOfTop100", population, "graduationRate", "graduationTime", "graduationDelay", "studentCost", "governmentFund", "GDP", year) FROM stdin;
7216000	5310000	1906000	\N	\N	\N	\N	\N	\N	119	0	81916871	\N	\N	\N	1050	\N	27956	2018
6306000	5064000	1242000	\N	\N	\N	\N	\N	\N	114	0	79512426	\N	\N	\N	\N	\N	26252	2016
5887000	4739000	1148000	\N	\N	\N	\N	\N	\N	108	0	78271472	\N	\N	\N	\N	\N	25710	2015
7012000	5239000	1773000	\N	\N	\N	\N	\N	\N	117	0	80745020	\N	\N	\N	\N	\N	27916	2017
5500000	4427000	1073000	\N	\N	\N	\N	\N	\N	104	0	77040234	\N	\N	\N	\N	\N	24029	2014
5103000	4296000	807000	\N	\N	\N	\N	\N	\N	100	0	75787624	\N	\N	\N	\N	\N	22310	2013
4889000	4140000	749000	\N	\N	\N	\N	\N	\N	98	0	74569217	\N	\N	\N	\N	\N	20639	2012
4763000	4075000	688000	\N	\N	\N	\N	\N	\N	95	0	73409545	\N	\N	\N	\N	\N	19660	2011
4059000	3421000	638000	\N	\N	\N	\N	\N	\N	88	0	71339634	\N	\N	\N	\N	\N	15486	2009
4384000	3689000	695000	\N	\N	\N	\N	\N	\N	90	0	72396041	\N	\N	\N	\N	\N	17425	2010
\.


--
-- Data for Name: USA; Type: TABLE DATA; Schema: public; Owner: team7admin
--

COPY public."USA" ("numOfStudents", "numOfMaleStudents", "numOfFemaleStudents", "numOfEmployees", "numOfMaleEmployees", "numOfFemaleEmployees", "numOfGraduates", "numOfMaleGraduates", "numOfFemaleGraduates", "numOfUniversities", "numOfTop100", population, "graduationRate", "graduationTime", "graduationDelay", "studentCost", "governmentFund", "GDP", year) FROM stdin;
20688000	8962000	11726000	3976901	1713000	2263901	3941000	837000	1126000	\N	31	327350000	\N	\N	\N	7054	389000000000	59927	2018
20413000	8869000	11544000	3970842	1812000	2158842	3947720	836050	1119990	4830	31	325330000	\N	\N	\N	6818	372000000000	57904	2017
19977270	8855000	11330000	3981632	1482000	2499632	3892870	821750	1099000	4360	32	323220000	49	\N	\N	6612	354780000000	56803	2016
19977270	8721403	11255867	4128462	1373000	2755462	3846660	812690	1082280	4583	30	320930000	48	\N	\N	6370	335630000000	55032	2015
20207369	8797061	11410308	3969226	1810000	2159226	380747	801910	1068250	4627	31	318620000	48	\N	\N	6120	323890000000	53106	2014
20376677	8861197	11515480	3967124	1738000	2229124	3774550	787410	1052970	4724	30	316330000	48	\N	\N	5899	311420000000	51603	2013
21019438	9045759	11973679	3795744	1625000	2170744	3350680	706660	943260	4599	29	309740000	49	\N	\N	4763	281390000000	47001	2010
20644478	8919006	11725472	3921675	1912000	2009675	3740070	765770	1026390	4726	30	314160000	48	\N	\N	5563	305540000000	49883	2012
21010590	9034256	11976334	3893998	1828000	2065998	3554310	734160	981890	4706	29	311940000	49	\N	\N	5075	296860000000	48466	2011
20313594	8732953	11580641	3210000	1719000	1491000	3205290	685420	915980	4495	\N	307235000	49	\N	\N	4512	273020000000	48401	2009
\.


--
-- Data for Name: Years; Type: TABLE DATA; Schema: public; Owner: team7admin
--

COPY public."Years" (year) FROM stdin;
2009
2010
2011
2012
2013
2014
2015
2016
2017
2018
\.


--
-- Name: CSF CSF_pkey; Type: CONSTRAINT; Schema: public; Owner: team7admin
--

ALTER TABLE ONLY public."CSF"
    ADD CONSTRAINT "CSF_pkey" PRIMARY KEY (id);


--
-- Name: Countries Countries_pkey; Type: CONSTRAINT; Schema: public; Owner: team7admin
--

ALTER TABLE ONLY public."Countries"
    ADD CONSTRAINT "Countries_pkey" PRIMARY KEY (id);


--
-- Name: Italy Italy_year_unique; Type: CONSTRAINT; Schema: public; Owner: team7admin
--

ALTER TABLE ONLY public."Italy"
    ADD CONSTRAINT "Italy_year_unique" UNIQUE (year);


--
-- Name: KPI KPI_pkey; Type: CONSTRAINT; Schema: public; Owner: team7admin
--

ALTER TABLE ONLY public."KPI"
    ADD CONSTRAINT "KPI_pkey" PRIMARY KEY (id);


--
-- Name: Years Years_pkey; Type: CONSTRAINT; Schema: public; Owner: team7admin
--

ALTER TABLE ONLY public."Years"
    ADD CONSTRAINT "Years_pkey" PRIMARY KEY (year);


--
-- Name: Analytics_KPI_id_fkey; Type: INDEX; Schema: public; Owner: team7admin
--

CREATE INDEX "Analytics_KPI_id_fkey" ON public."Analytics" USING btree ("KPI_id");


--
-- Name: Analytics_country_id_fkey; Type: INDEX; Schema: public; Owner: team7admin
--

CREATE INDEX "Analytics_country_id_fkey" ON public."Analytics" USING btree (country_id);


--
-- Name: Italy_year_fkey; Type: INDEX; Schema: public; Owner: team7admin
--

CREATE INDEX "Italy_year_fkey" ON public."Italy" USING btree (year);


--
-- Name: Netherlands_year_fkey; Type: INDEX; Schema: public; Owner: team7admin
--

CREATE INDEX "Netherlands_year_fkey" ON public."Netherlands" USING btree (year);


--
-- Name: Turkey_year_fkey; Type: INDEX; Schema: public; Owner: team7admin
--

CREATE INDEX "Turkey_year_fkey" ON public."Turkey" USING btree (year);


--
-- Name: USA_year_fkey; Type: INDEX; Schema: public; Owner: team7admin
--

CREATE INDEX "USA_year_fkey" ON public."USA" USING btree (year);


--
-- Name: a_CSF_fkey; Type: INDEX; Schema: public; Owner: team7admin
--

CREATE INDEX "a_CSF_fkey" ON public."Analytics" USING btree ("CSF_id");


--
-- Name: fki_CSF_fkey; Type: INDEX; Schema: public; Owner: team7admin
--

CREATE INDEX "fki_CSF_fkey" ON public."KPI" USING btree ("CSF_id");


--
-- Name: Analytics Analytics_CSF_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: team7admin
--

ALTER TABLE ONLY public."Analytics"
    ADD CONSTRAINT "Analytics_CSF_id_fkey" FOREIGN KEY ("CSF_id") REFERENCES public."CSF"(id);


--
-- Name: Analytics Analytics_KPI_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: team7admin
--

ALTER TABLE ONLY public."Analytics"
    ADD CONSTRAINT "Analytics_KPI_id_fkey" FOREIGN KEY ("KPI_id") REFERENCES public."KPI"(id);


--
-- Name: Analytics Analytics_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: team7admin
--

ALTER TABLE ONLY public."Analytics"
    ADD CONSTRAINT "Analytics_country_id_fkey" FOREIGN KEY (country_id) REFERENCES public."Countries"(id);


--
-- Name: KPI CSF_fkey; Type: FK CONSTRAINT; Schema: public; Owner: team7admin
--

ALTER TABLE ONLY public."KPI"
    ADD CONSTRAINT "CSF_fkey" FOREIGN KEY ("CSF_id") REFERENCES public."CSF"(id);


--
-- Name: Italy Italy_year_fkey; Type: FK CONSTRAINT; Schema: public; Owner: team7admin
--

ALTER TABLE ONLY public."Italy"
    ADD CONSTRAINT "Italy_year_fkey" FOREIGN KEY (year) REFERENCES public."Years"(year);


--
-- Name: Netherlands Netherlands_year_fkey; Type: FK CONSTRAINT; Schema: public; Owner: team7admin
--

ALTER TABLE ONLY public."Netherlands"
    ADD CONSTRAINT "Netherlands_year_fkey" FOREIGN KEY (year) REFERENCES public."Years"(year);


--
-- Name: Turkey Turkey_year_fkey; Type: FK CONSTRAINT; Schema: public; Owner: team7admin
--

ALTER TABLE ONLY public."Turkey"
    ADD CONSTRAINT "Turkey_year_fkey" FOREIGN KEY (year) REFERENCES public."Years"(year);


--
-- Name: USA USA_year_fkey; Type: FK CONSTRAINT; Schema: public; Owner: team7admin
--

ALTER TABLE ONLY public."USA"
    ADD CONSTRAINT "USA_year_fkey" FOREIGN KEY (year) REFERENCES public."Years"(year);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: team7admin
--

REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO team7admin;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

