library(shinyjs)
library(shiny)
library(shinydashboard)
library(ggplot2)
library(bsplus)
library(shinyWidgets)
library(flexdashboard)
source('./Queries.R')

shinyUI(
  navbarPage("Higher Education Comparison",
             tabPanel("Summary",
                      fluidRow(
                      box("Education is centered around teaching people how to function properly in a society. It is a vital part of society characterizing its thriving in the future and accordingly has a social responsibility. The educational division persistently experiences changes caused by technological, innovation, economic or legislation changes. Education organizations set new business objectives to react on these. Key Performance Indicators are utilized to gauge business processes that are focused on the achievement of the set business objectives dependent on current challenges. KPIs should correspond with these challenges.",title="Strategy",width=6),
                      box(h4("Low Gender Gap in the universities"),
                          "1. Low Gender Gap among Students",
                          br(),
                          "2. Low Gender Gap among Employees",
                          h4("High International Openness"),
                          "1. Number of International Students",
                          br(),
                          "2. Number of International Employees",
                          h4("Low Higher Education Cost"),
                          "1. The relation of GDP to the education spending",
                          br(),
                          "2. Education spending covered by students and their families"
                          ,title="The focus will be within these Critical Success Factors and Key Performance Indicators:",width=6)),
                      fluidRow(
                        box("The selected countries are The Netherlands, Italy, Turkey and the United States. The country selection provides a good insight into the current education differences in developed and developing countries. The Netherlands and Italy are chosen as developed European countries. Another developed country but with a different cultural, the United States is chosen for a good comparison between different continents. Turkey, on the other hand, is chosen because it will be representing the education level in a developing country. With those four countries it is possible to give insight in the difference in the high education level between developed countries and the differences between developed and developing countries.",title="Selected Countries",width=12)
                        
                      )),
             tabPanel("Analytics",
                      useShinyjs(),
                      tabsetPanel(
                        tabPanel("Low Gender Gap in Universities",
                                 fluidRow(
                                   box(
                                     sliderInput('yearSliderGauge1', 'Select Years', sep="",
                                                 min=2009, max=2018, step=1, value =c(2009,2018)),
                                   column(3,flexdashboard::gaugeOutput("gaugeNLD01")),
                                   column(3,flexdashboard::gaugeOutput("gaugeITA01")),
                                   column(3,flexdashboard::gaugeOutput("gaugeUSA01")),
                                   column(3,flexdashboard::gaugeOutput("gaugeTUR01")), title="Percentage of Female Students(Mean)",width=12)
                                 ),
                                 fluidRow(
                                   box(
                                     sliderInput('yearSliderGauge2', 'Select Years', sep="",
                                                 min=2009, max=2018, step=1, value =c(2009,2018)),
                                     column(3,flexdashboard::gaugeOutput("gaugeNLD02")),
                                     column(3,flexdashboard::gaugeOutput("gaugeITA02")),
                                     column(3,flexdashboard::gaugeOutput("gaugeUSA02")),
                                     column(3,flexdashboard::gaugeOutput("gaugeTUR02")), title="Percentage of Female Staff(Mean)",width=12)
                                 ),
                                 fluidRow(box(
                                 column(4, inputPanel(selectInput("selectGraph", label = h4("Select a graph type to show:"), 
                                                                  choices = list("Comparison graph" = 1, "Dendrogram Netherlands" = 2, "Dendrogram Italy" = 3, "Dendrogram USA" = 2, "Dendrogram Turkey" = 5), 
                                                                 selected = 1),
                                                      checkboxGroupInput("countryCSF1",h4('Select Countries'),
                                                                         choices = list("Netherlands" = 1, 
                                                                                        "Italy" = 2, 
                                                                                        "USA" = 3,
                                                                                        "Turkey" = 4),
                                                                         selected = c(1,2,3,4))
                                                      
                                                      
                                 )
                                 ),
                                 column(8, 
                                        plotlyOutput("firstAnalytics01")
                                 ),title="Analysis of Low Gender Gap in Universities",width=12))),
                        tabPanel("High International Openness",
                                 
                                 column(3, inputPanel(checkboxGroupInput("countryCSF2",h4('Select Countries'),
                                                                         choices = list("Netherlands" = 1, 
                                                                                        "Italy" = 2, 
                                                                                        "USA" = 3,
                                                                                        "Turkey" = 4),
                                                                         selected = c(1,2,3,4)),
                                                      sliderInput('yearSlider2', 'Select Years', sep="",
                                                                  min=2009, max=2018, step=1, value =c(2009,2018))
                                                      
                                                      
                                 )),
                                 column(9, plotlyOutput("secondAnalytics01"))),
                        tabPanel("Low Higher Education Cost",
                                 fluidRow(
                                   box(
                                     sliderInput('yearSliderGauge3', 'Select Years', sep="",
                                                 min=2009, max=2018, step=1, value =c(2009,2018)),
                                     column(3,flexdashboard::gaugeOutput("gaugeNLD03")),
                                     column(3,flexdashboard::gaugeOutput("gaugeITA03")),
                                     column(3,flexdashboard::gaugeOutput("gaugeUSA03")),
                                     column(3,flexdashboard::gaugeOutput("gaugeTUR03")), title="The relation of GDP to the education spending(Mean)",width=12)
                                 ),
                                 fluidRow(
                                   box(
                                     sliderInput('yearSliderGauge4', 'Select Years', sep="",
                                                 min=2009, max=2018, step=1, value =c(2009,2018)),
                                     column(3,flexdashboard::gaugeOutput("gaugeNLD04")),
                                     column(3,flexdashboard::gaugeOutput("gaugeITA04")),
                                     column(3,flexdashboard::gaugeOutput("gaugeUSA04")),
                                     column(3,flexdashboard::gaugeOutput("gaugeTUR04")), title="Percentage of education spending covered by students and their families(Mean)",width=12)
                                 ),
                                 fluidRow(box(
                                 column(3, inputPanel(checkboxGroupInput("countryCSF3",h4('Select Countries to base Regression Line'),
                                                                         choices = list("Netherlands" = 1, 
                                                                                        "Italy" = 2, 
                                                                                        "USA" = 3,
                                                                                        "Turkey" = 4),
                                                                         selected = c(1,3))
                                                      
                                 )),
                                 column(9, plotlyOutput("thirdAnalytics01")),title="Self-Clustered Education Cost",width=12))))),
             tabPanel("Exploration",
                      useShinyjs(),
                      fluidRow(
                        column(3,
                               verbatimTextOutput("currentPlot"),
                               uiOutput("actionLinks")
                        ),
                        column(6, 
                               fluidRow(
                                 plotlyOutput("firstPlot")
                               ),
                               fluidRow(
                                 plotlyOutput("secondPlot")
                               )
                        ),
                        column(3,
                               inputPanel(
                                 sliderInput('yearSlider1', 'Select Years', sep="",
                                                                  min=2009, max=2018, step=1, value =c(2009,2018)),
                                                      (checkboxGroupInput("country1",h4('Select Countries'),
                                                                          choices = list("Netherlands" = 1, 
                                                                                         "Italy" = 2, 
                                                                                         "USA" = 3,
                                                                                         "Turkey" = 4),
                                                                          selected = 1)),
                                                      
                                                      selectInput("plotType1", label = h4("Select Chart Type"), 
                                                                  choices = list("Donut" = 1, "Line" = 2, "Line(w/ markers)" = 3, "Line(only markers)" = 4), 
                                                                  selected = 2),
                                 downloadButton("download", "Download Data"))))
             ),
             tabPanel("Contact",
                      box(
                        h5("Second version of Team Seven(7) project:"),
                        htmlOutput("text"),title="Contact Us",width=12))
             
  )
)
